﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdSelection : MonoBehaviour
{

    public Camera CameraFacing;
    public float screenX;
    public float screenY;
    public float screenZ;
    public float invisibleX = -3000;
    GameObject selection;
    GameObject selected;
    int index = 0;
    bool isVisible = false;
    float timeRemaining;

    void Start() {
        selection = GameObject.FindGameObjectWithTag("Selected");
    }

    public int GetIndex() {
        return index;
    }

    public void DisableSelection() {
        selection.SetActive(false);
    }

    void Update() {

        selected = GameObject.Find("UI" + index.ToString());

        if (isVisible) {
            Vector3 screenPos = CameraFacing.ScreenToWorldPoint(new Vector3(screenX, screenY, screenZ));
            transform.position = screenPos;

            Vector3 pos = new Vector3(selected.transform.position.x, selected.transform.position.y, (selected.transform.position.z + 0.1f));
            selection.transform.position = pos;
        }

        if (!isVisible) {
            Vector3 screenPos = CameraFacing.ScreenToWorldPoint(new Vector3(invisibleX, screenY, screenZ));
            transform.position = screenPos;

            Vector3 pos = new Vector3(selected.transform.position.x, selected.transform.position.y, (selected.transform.position.z + 0.1f));
            selection.transform.position = pos;
        }

        if (timeRemaining > 0) {
            isVisible = true;
            timeRemaining -= Time.deltaTime;
        }

        else
            isVisible = false;

        if (Input.GetKeyDown("up") || OVRInput.GetDown(OVRInput.Button.DpadUp)) {
            if (index < 2)
                index++;

            else index = 0;

            timeRemaining = 2;
        }

        if (Input.GetKeyDown("down") || OVRInput.GetDown(OVRInput.Button.DpadDown)) {
            if (index > 0)
                index--;

            else index = 2;

            timeRemaining = 2;
        }
    }
}


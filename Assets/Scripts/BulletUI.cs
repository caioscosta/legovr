﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletUI : MonoBehaviour {

    public Camera CameraFacing;
    public float screenX = 650f;
    public float screenY = 350f;
    public float screenZ = 4f;
    bool isDrawing = true;
    private float rotationX = 0.0f;
    private float rotationY = 1f;
    private float rotationZ = 0.0f;
    Vector3 screenPos;
    GameObject selected;
    GameObject prefab;
    GameObject instance;
    bool hasSwitched;
    int index;


    void Start() {
        selected = GameObject.Find("Bird" + index.ToString());
        InstanciateBird();
    }

    void Update() {

        if(hasSwitched)
            selected = GameObject.Find("Bird" + index.ToString());

        screenPos = CameraFacing.ScreenToWorldPoint(new Vector3(screenX, screenY, screenZ));

        isDrawing = instance.GetComponent<MeshRenderer>().enabled;

        instance.transform.position = screenPos;
        instance.transform.Rotate(rotationX, rotationY, rotationZ);

        if (Input.GetKeyDown("up") || OVRInput.GetDown(OVRInput.Button.DpadUp)) {
            if (index < 2) {
                index++;
                selected = GameObject.Find("Bird" + index.ToString());
            }

            else {
                index = 0;
                selected = GameObject.Find("Bird" + index.ToString());
            }

            Destroy(instance);
            InstanciateBird();
            hasSwitched = true;
        }

        if (Input.GetKeyDown("down") || OVRInput.GetDown(OVRInput.Button.DpadDown)) {
            if (index > 0) {
                index--;
                selected = GameObject.Find("Bird" + index.ToString());
            }

            else {
                index = 2;
                selected = GameObject.Find("Bird" + index.ToString());
            }


            Destroy(instance);
            InstanciateBird();
            hasSwitched = true;
        }

        if (Input.GetKeyDown("space") || OVRInput.GetDown(OVRInput.Button.One)) {
            if (instance.GetComponent<MeshRenderer>().enabled) {
                if (GameObject.Find("Player").GetComponent<LaunchBird>().GetAmmo(index) > 0){
                    instance.GetComponent<MeshRenderer>().enabled = false;
                    StartCoroutine(WaitForShot(1.5f));
                }
            }
        }
    }

    public void DisableUI(){
        GameObject.Find("BirdUI").transform.parent.gameObject.SetActive(false);
    }

    void InstanciateBird() {
        
        prefab = selected;
        instance = Instantiate(prefab);
        instance.tag = "Active";
        instance.name = "Muzzle";
        instance.transform.parent = gameObject.transform;

        instance.transform.Rotate(0f, 180f, 0f);
        screenPos = CameraFacing.ScreenToWorldPoint(new Vector3(screenX, screenY, screenZ));
        instance.transform.position = screenPos;
        instance.transform.Rotate(rotationX, rotationY, rotationZ);
        instance.GetComponent<MeshRenderer>().enabled = isDrawing;
    }

    IEnumerator WaitForShot(float waitTime) {
        yield return new WaitForSeconds(waitTime);
        instance.GetComponent<MeshRenderer>().enabled = true;
    }
}


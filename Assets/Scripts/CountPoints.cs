﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountPoints : MonoBehaviour {

    public Text score;
    public int pigPontuation = 10000;
    public int brickPontuation = 50;
    public int pigs;
	int points;
	
	void Update () {
        score.text = "Pontuação: " + points;
	}

    public void AddScore(string type){
        if (type == "brick")
            points += brickPontuation;
        if (type == "pig") {
            points += pigPontuation;
            pigs++;
        }
    }

    public int GetPoints(){
        return points;
    }

    public int GetPigPontuation() {
        return pigPontuation;
    }

    public void DisableScore(){
        score.enabled = false;
    }
}

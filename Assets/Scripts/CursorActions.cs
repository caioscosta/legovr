﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CursorActions : MonoBehaviour {

    public Camera CameraFacing;


    void Update() {
        RaycastHit hit;
        if (Physics.Raycast(new Ray(CameraFacing.transform.position,
            CameraFacing.transform.rotation * Vector3.forward), out hit)) {
            Debug.Log(hit.transform.tag);
            if (OVRInput.GetDown(OVRInput.Button.One) || Input.GetKeyDown("space")) {
                
                if (hit.transform.tag == "PlayButton") { 
                    Initiate.Fade("Game", Color.black, 5f);
                }

                if (hit.transform.tag == "CreditsButton") {
                    Initiate.Fade("Credits", Color.black, 5f);
                }

                if (hit.transform.tag == "MainMenu") {
                    SceneManager.LoadScene("MainMenu");
                }

                if (hit.transform.tag == "Exit") {
                    Application.Quit();
                }

            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyBehaviour : MonoBehaviour
{

    public float pigResistance;
    bool destroyed;

    void Start(){
        GetComponent<ParticleSystem>().Stop();
    }

    void DecreaseResistance(Collision collision){

        //Resistance decreased by birds
        if (collision.gameObject.tag == "Red" || collision.gameObject.tag == "Yellow")
            pigResistance -= 10f;
        if (collision.gameObject.tag == "Black")
            pigResistance -= 15f;

        //Resistance decreased by active birds
        if (collision.gameObject.tag == "YellowGlide")
            pigResistance -= 20f;
        if (collision.gameObject.tag == "BlackExplosion")
            pigResistance -= 30f;

        //Resistance decreased by structures
        if (collision.gameObject.tag == "DetachedBrick")
            pigResistance -= 5f;
        if (collision.gameObject.tag == "Ground" && gameObject.tag != "GroundBase")
            pigResistance -= 100f;

        //Execute detach
        if (pigResistance <= 0 && !destroyed) {
            StartCoroutine(DestroyPig());
            GameObject.Find("GameManager").GetComponent<GameLoop>().CountEnemies();
            destroyed = true;
        }
    }

    IEnumerator DestroyPig() {
        GameObject.Find("Canvas").GetComponent<CountPoints>().AddScore("pig");
        yield return new WaitForSeconds(Random.Range(2.0f, 4.0f));
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<ParticleSystem>().Play();
        yield return new WaitForSeconds(6f);
        Destroy(gameObject);
    }

    public void OnCollisionEnter(Collision collision) {
        DecreaseResistance(collision);
    }
}

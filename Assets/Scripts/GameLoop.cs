﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class GameLoop : MonoBehaviour {

    public Camera CameraFacing;

    public SpriteRenderer[] stars = new SpriteRenderer[3];
    public Text finalScore;

    public GameObject gameOverContainer;
    public GameObject pauseContainer;
    public GameObject dialogContainer;

    public string gameState;

    GameObject selection;
    GameObject ui;
    GameObject overlay;
    GameObject crosshair;
    GameObject reticle;
    GameObject bullet;

    string points;
    float timeout;
    int myScore;
    int pigsDefeated = 0;
    int[] arr = new int[3];
    int[] digits = new int[6];
    int types;
    int ammo;

	// Use this for initialization
	void Start () {

        selection = GameObject.Find("Selection");
        ui = GameObject.Find("InGameCanvas");
        overlay = GameObject.Find("Overlay");
        crosshair = GameObject.Find("Mira");
        reticle = GameObject.Find("Reticle");
        bullet = GameObject.Find("BirdUI");

        int quantity = 0;
        types = arr.Length;

        for (int i = 0; i < types; i++) {
            quantity += GameObject.Find("Player").GetComponent<LaunchBird>().GetAmmo(i);
        }

	}
	
	// Update is called once per frame
	void Update () {

        if(timeout > 0)
            timeout -= Time.deltaTime;
        
        ammo = CountAmmo();

        if (ammo <= 0)
            StartCoroutine(ShowScore());

        if (pigsDefeated > 3) {
            StartCoroutine(ShowScore());
        }

        if(Input.GetKeyDown("z") || (Input.GetKeyDown(KeyCode.Escape))){

            if (!gameOverContainer.activeSelf) {
                if (dialogContainer.activeSelf) {
                    if (!pauseContainer.activeSelf)
                        PauseGame("pause");
                }

                else {
                    if (pauseContainer.activeSelf)
                        ContinueGame();
                    else
                        PauseGame("pause");
                }
            }
        }

        RaycastHit hit;
        if (Physics.Raycast(new Ray(CameraFacing.transform.position,
            CameraFacing.transform.rotation * Vector3.forward), out hit)) {
            
            if (Input.GetKeyDown("space") || OVRInput.GetDown(OVRInput.Button.One)) {

				if (hit.transform.tag == "Restart") {
                    Time.timeScale = 1;
					Initiate.Fade("Game", Color.black, 3f);
				}

                if (hit.transform.tag == "MainMenu") {
                    Time.timeScale = 1;
                    Initiate.Fade("MainMenu", Color.black, 3f);
                }

				if (hit.transform.tag == "Exit") {
                    dialogContainer.SetActive(true);
                    pauseContainer.SetActive(false);
                    GameObject.Find("dialogMessage").GetComponent<Text>().text = "Sair para o menu principal?";
				}

                if (hit.transform.tag == "Back") {
                    PauseGame("pause");
                }

                if (hit.transform.tag == "Continue") {
                    ContinueGame();
                }

                if (hit.transform.tag == "Quit") {
                    Time.timeScale = 1;
                    Initiate.Fade("MainMenu", Color.black, 3f);
                }

            }
        }
	}

    public void CountEnemies() {
        pigsDefeated += 1;
    }

    void PauseGame(string kind) {
        
        GameObject.Find("Player").GetComponent<LaunchBird>().DisableShot();

        if (kind == "pause") {
            Time.timeScale = 0;
            dialogContainer.SetActive(false);
            pauseContainer.SetActive(true);
        }

        if (kind == "win") {
            pauseContainer.SetActive(false);
            gameOverContainer.SetActive(true);
        }

        selection.SetActive(false);
        ui.SetActive(false);
        bullet.SetActive(false);
        overlay.GetComponent<Canvas>().enabled = true;
        reticle.GetComponent<Canvas>().enabled = true;
        crosshair.GetComponent<Canvas>().enabled = false;

    }

    void ContinueGame() {

        Time.timeScale = 1;

        GameObject.Find("Player").GetComponent<LaunchBird>().EnableShot();

        selection.SetActive(true);
        ui.SetActive(true);
        bullet.SetActive(true);
        pauseContainer.SetActive(false);
        overlay.GetComponent<Canvas>().enabled = false;
        reticle.GetComponent<Canvas>().enabled = false;
        crosshair.GetComponent<Canvas>().enabled = true;

    }

    IEnumerator ShowScore(){

        yield return new WaitForSeconds(5f);
        int[] display = StringToDigits();

        //Get final score
        myScore = GameObject.Find("Canvas").GetComponent<CountPoints>().GetPoints();

        //Pause Game
        PauseGame("win");

        //Animate score number
        //StartCoroutine(AnimateScore());
        finalScore.text = ("Pontuação" + '\n' + '\n' + myScore);

        //Shine stars
        StartCoroutine(ShineStars());

    }
     
    int[] StringToDigits() {
        
        string score = myScore.ToString();
        List<int> myList = new List<int>();
        int index = score.Length;

        for (int i = index; i < 6; i++){
            myList.Add(0);
        }

        for (int i = 0; i < index; i++){
            char curDigit = score[i];
            myList.Add((int)Char.GetNumericValue(curDigit));
        }

        digits = myList.ToArray();
        return digits;

    }

    IEnumerator AnimateScore(){
        yield return new WaitForSeconds(2f);
        int animScore = 0;

        animScore = animScore + UnityEngine.Random.Range(1, 100000);
        if (animScore > 900000)
            animScore = animScore - 900000;
        points = animScore.ToString();
        Debug.Log(points);
    }

    IEnumerator ShineStars(){

        yield return new WaitForSeconds(1f);

        SpriteRenderer[] levelStar = new SpriteRenderer[3];
        levelStar[0] = GameObject.Find("star1").GetComponent<SpriteRenderer>();
        levelStar[1] = GameObject.Find("star2").GetComponent<SpriteRenderer>();
        levelStar[2] = GameObject.Find("star3").GetComponent<SpriteRenderer>();

        int totalEnemies = GameObject.FindGameObjectsWithTag("Enemy").Length;
        int enemyPontuation = GameObject.Find("Canvas").GetComponent<CountPoints>().GetPigPontuation();


		if (myScore >= (totalEnemies * enemyPontuation) * 0.33f) {
			levelStar[0].color = Color.white;
            Debug.Log("Color 1");
            Debug.Log(points);
			//play particle
			yield return new WaitForSeconds(0.25f);
		}
		
        if (myScore >= (totalEnemies * enemyPontuation) * 0.66f) {
			levelStar[1].color = Color.white;
            Debug.Log("Color 2");
			//play particle
            yield return new WaitForSeconds(0.25f);
		}

        if (myScore >= totalEnemies * enemyPontuation) {
            levelStar[2].color = Color.white;
            Debug.Log("Color 3");
            //play particle
            yield return new WaitForSeconds(0.25f);
        }

    }

    int CountAmmo() {
        int quantity = 0;
        types = arr.Length;

        for (int i = 0; i < types; i++){
            quantity += GameObject.Find("Player").GetComponent<LaunchBird>().GetAmmo(i);
        }

        return quantity;
    }
        
}

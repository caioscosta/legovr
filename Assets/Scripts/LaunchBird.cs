﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LaunchBird : MonoBehaviour {

    public Transform target;
    Transform start;
    public float mass = 500f;
    public float shootAngle = 67.5f;
    public float acceleration;
    bool canShot = true;
    GameObject selected;
    GameObject prefab;
    int index = 0;
    public int[] ammo = {4, 3, 3};

    void Start() {
        selected = GameObject.Find("Bird" + index.ToString());
        prefab = selected;
    }

    void Update () {

        selected = GameObject.Find("Bird" + index.ToString());

        prefab = selected;
        start = GameObject.FindGameObjectWithTag("Active").transform;

        if (canShot) {
            if (Input.GetKeyDown("space") || OVRInput.GetDown(OVRInput.Button.One)) {
                if (index == 0)
                    prefab.tag = "Red";
                if (index == 1)
                    prefab.tag = "Yellow";
                if (index == 2) 
                    prefab.tag = "Black";

                Shot(prefab, start, target, shootAngle);
                StartCoroutine(WaitForShot(1.5f));
            }
        }

        if (Input.GetKeyDown("up") || OVRInput.GetDown(OVRInput.Button.DpadUp)) {
            if (index < 2)
                index++;

            else index = 0;
        }

        if (Input.GetKeyDown("down") || OVRInput.GetDown(OVRInput.Button.DpadDown))
        {
            if (index > 0)
                index--;

            else index = 2;
        }
    }

    public void DisableShot(){
        GetComponent<LaunchBird>().enabled = false;
    }

    public void EnableShot(){
        GetComponent<LaunchBird>().enabled = true;
    }

    public Vector3 BallisticVel(Transform target, float angle) {
        Vector3 direction = target.position - start.position;
        float height = direction.y;
        direction.y = 0f;
        float distance = direction.magnitude;
        float radAngle = angle * Mathf.Deg2Rad;
        direction.y = distance * Mathf.Tan(radAngle);
        distance += height / Mathf.Tan(radAngle);
        float velocity = Mathf.Sqrt(distance * Physics.gravity.magnitude / Mathf.Sin(2 * radAngle));

        return velocity * direction.normalized;
    }

    public int GetAmmo(int index){
        return ammo[index];
    }

    public int GetIndex(){
        return index;
    }

    IEnumerator WaitForShot(float waitTime) {
        yield return new WaitForSeconds(waitTime);
        canShot = true;
    }

    void Shot(GameObject bird, Transform start, Transform target, float shootAngle) {
        if(ammo[index] > 0) {
            GameObject bullet = Instantiate(bird, start.position, Quaternion.identity);
            bullet.transform.LookAt(target);

            bullet.transform.localScale *= 3f;

            SphereCollider bulletCld = bullet.AddComponent<SphereCollider>();
            Rigidbody bulletRb = bullet.AddComponent<Rigidbody>();
            //Vector3 myVel = BallisticVel(target, shootAngle);
            //myVel.x = myVel.x * 4f; myVel.y = myVel.y / 2f; myVel.z = myVel.z * 4f;
            bulletRb.mass = mass;
            bulletRb.AddForce(target.forward * acceleration);
            Destroy(bullet, 5);
            canShot = false;
            ammo[index]--;
        }
    }

}

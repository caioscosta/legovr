﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceRotation : MonoBehaviour {

    public float rotationSpeed;
    float secondsCount;
    
    void Start() {
        if (gameObject.tag != "Birds") {
            if (System.Math.Abs(rotationSpeed) <= 0)
                rotationSpeed = Random.Range(0, 0.6f);
        }
    }

    void Update() {
        transform.Rotate(rotationSpeed, 0, 0);
        secondsCount += Time.deltaTime;
        if (secondsCount < 10)
            gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, Random.Range(0, 0.15f));
        else
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReticleManager : MonoBehaviour
{

    public Camera CameraFacing;
    public float distance;
    Vector3 originalScale;

    void Start() {
        originalScale = transform.localScale;
    }

    void Update(){
        RaycastHit hit;
        if (Physics.Raycast(new Ray(CameraFacing.transform.position,
            CameraFacing.transform.rotation * Vector3.forward), out hit)) {
            distance = hit.distance;
        } else {
            distance = CameraFacing.farClipPlane * 0.85f;
        }

        transform.position = CameraFacing.transform.position +
            CameraFacing.transform.rotation * Vector3.forward * distance;
        
        transform.LookAt(CameraFacing.transform.position);
        transform.Rotate(0.0f, 180.0f, 0.0f);

        if(distance < 10) {
            distance *= 1 + 5 * Mathf.Exp (-distance);
        }

        if (distance > CameraFacing.farClipPlane)
            distance = CameraFacing.farClipPlane - 200f;

        transform.localScale = originalScale * distance;

    }

}
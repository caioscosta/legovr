﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionBehaviour : MonoBehaviour {

    public GameObject selection;
	
	void Update () {

        if (Input.GetKeyDown("up") || OVRInput.GetDown(OVRInput.Button.DpadUp)) {
            
        }

        if (Input.GetKeyDown("down") || OVRInput.GetDown(OVRInput.Button.DpadDown)) {
        }
    }

    IEnumerator WaitForTime(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
    }
}

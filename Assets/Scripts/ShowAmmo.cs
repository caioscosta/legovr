﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowAmmo : MonoBehaviour {
    Transform start;
    GameObject selected;
    GameObject prefab;
    public Text ammo;
    int currentIndex;

    void Start() {
        currentIndex = GameObject.Find("Player").GetComponent<LaunchBird>().GetIndex();
        selected = GameObject.Find("Bird" + currentIndex.ToString());
        prefab = selected;
    }

    void Update() {

        selected = GameObject.Find("Bird" + currentIndex.ToString());

        prefab = selected;

        if(ammo.IsActive())
            start = GameObject.FindGameObjectWithTag("Active").transform;

        currentIndex = GameObject.Find("Player").GetComponent<LaunchBird>().GetIndex();
        ammo.text = ("x" + GameObject.Find("Player").GetComponent<LaunchBird>().GetAmmo(currentIndex).ToString());
    }

    public void DisableAmmo() {
        ammo.enabled = false;
    }
}
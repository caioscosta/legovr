﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Splashes : MonoBehaviour {

    public GameObject[] splash;
    public GameObject overlay;

    private YieldInstruction fadeInstruction = new WaitForEndOfFrame();
    private int index;


    Color olColor;
    float r, g, b, a;
    bool isFadingIn;
    bool hasEnded = true;
    public float fadeTime = 3.0f;

	void Start() {
        StartCoroutine(FadeScript());
	}

    void Update() {
    }

    IEnumerator FadeScript(){
        int i = 0;
        while (i < splash.Length) {
            Debug.Log("Iteration: " + i + " Splash size: " + splash.Length + " Index: " + index);
            StartCoroutine(FadeIn());
            yield return new WaitForSeconds(2 * fadeTime + 1.25f);
            i++;
        }
        SceneManager.LoadScene("MainMenu");
    }


    IEnumerator FadeIn() {

        splash[index].SetActive(true);

        float elapsedTime = 0.0f;
        olColor = new Color(0, 0, 0, 0);
        isFadingIn = true;
        while (elapsedTime < fadeTime) {
            yield return fadeInstruction;
            elapsedTime += Time.deltaTime;
            olColor.a = 1.0f - Mathf.Clamp01(elapsedTime / fadeTime);
            overlay.GetComponent<Image>().color = olColor;
        }
        isFadingIn = false;
        StartCoroutine(FadeOut());
    }

    IEnumerator FadeOut() {
        yield return new WaitForSeconds(1f);
        float elapsedTime = 0.0f;
        olColor = new Color(0, 0, 0, 0);
        while (elapsedTime < fadeTime) {
            yield return fadeInstruction;
            elapsedTime += Time.deltaTime;
            olColor.a = Mathf.Clamp01(elapsedTime / fadeTime);
            overlay.GetComponent<Image>().color = olColor;
        }

        hasEnded = true;

        splash[index].SetActive(false);

        index++;
        Debug.Log("Increased! " + index);

    }
        
}

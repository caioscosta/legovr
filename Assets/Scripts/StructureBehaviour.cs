﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StructureBehaviour : MonoBehaviour {

    public float structureResistance;
    public bool hit;
    public GameObject score;

    void Start(){
        GetComponent<ParticleSystem>().Stop();
    }

    void DecreaseResistance(Collision collision) {
    
        //Resistance decreased by birds
        if (collision.gameObject.tag == "Red" || collision.gameObject.tag == "Yellow")
            structureResistance -= 30f;
        if (collision.gameObject.tag == "Black")
            structureResistance -= 45f;
        
        //Resistance decreased by active birds
        if (collision.gameObject.tag == "YellowGlide")
            structureResistance -= 60f;
        if (collision.gameObject.tag == "BlackExplosion")
            structureResistance -= 90f;
        
        //Resistance decreased by structures
        if (collision.gameObject.tag == "DetachedBrick")
            structureResistance -= 5f;
        if (collision.gameObject.tag == "Ground" && gameObject.tag != "GroundBase") 
            structureResistance -= 100f;

        //Execute detach
        if (structureResistance <= 0) {
            DetachAll();
        }
    }

    void DetachAll(){
        transform.GetComponent<Rigidbody>().isKinematic = true;
        transform.GetComponent<Rigidbody>().useGravity = false;
        transform.GetComponent<BoxCollider>().enabled = false;
        foreach (Transform child in transform) {
            child.GetComponent<BoxCollider>().enabled = true;
            child.GetComponent<Rigidbody>().isKinematic = false;
            child.GetComponent<Rigidbody>().useGravity = true;
            child.tag = "DetachedBrick";
            GameObject.Find("Canvas").GetComponent<CountPoints>().AddScore("brick");
            GetComponent<ParticleSystem>().Play();
            //Destroy(child.gameObject, Random.Range(8f, 11f));
        }
    }

    public void OnCollisionEnter(Collision collision) {
        DecreaseResistance(collision);
    }
}

﻿// Compiled shader for Android

//////////////////////////////////////////////////////////////////////////
// 
// NOTE: This is *not* a valid shader file, the contents are provided just
// for information and for debugging purposes only.
// 
//////////////////////////////////////////////////////////////////////////
// Skipping shader variants that would not be included into build of current scene.

Shader "Unlit/Transparent" {
Properties {
 _MainTex ("Base (RGB) Trans (A)", 2D) = "white" { }
}
SubShader { 
 LOD 100
 Tags { "QUEUE"="Transparent" "IGNOREPROJECTOR"="true" "RenderType"="Transparent" }


 // Stats for Vertex shader:
 //         gles: 0 math, 1 texture
 Pass {
  Tags { "QUEUE"="Transparent" "IGNOREPROJECTOR"="true" "RenderType"="Transparent" }
  ZWrite Off
  Blend SrcAlpha OneMinusSrcAlpha
  //////////////////////////////////
  //                              //
  //      Compiled programs       //
  //                              //
  //////////////////////////////////
//////////////////////////////////////////////////////
No keywords set in this variant.
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 0 math, 1 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec2 xlv_TEXCOORD0;
void main ()
{
  lowp vec4 tmpvar_1;
  tmpvar_1 = texture2D (_MainTex, xlv_TEXCOORD0);
  gl_FragData[0] = tmpvar_1;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 0 math, 1 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec2 xlv_TEXCOORD0;
void main ()
{
  lowp vec4 tmpvar_1;
  tmpvar_1 = texture2D (_MainTex, xlv_TEXCOORD0);
  gl_FragData[0] = tmpvar_1;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 0 math, 1 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec2 xlv_TEXCOORD0;
void main ()
{
  lowp vec4 tmpvar_1;
  tmpvar_1 = texture2D (_MainTex, xlv_TEXCOORD0);
  gl_FragData[0] = tmpvar_1;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform     vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform     vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform     vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec2 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec2 vs_TEXCOORD0;
layout(location = 0) out mediump vec4 SV_Target0;
lowp vec4 u_xlat10_0;
void main()
{
    u_xlat10_0 = texture(_MainTex, vs_TEXCOORD0.xy);
    SV_Target0 = u_xlat10_0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform     vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform     vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform     vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec2 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec2 vs_TEXCOORD0;
layout(location = 0) out mediump vec4 SV_Target0;
lowp vec4 u_xlat10_0;
void main()
{
    u_xlat10_0 = texture(_MainTex, vs_TEXCOORD0.xy);
    SV_Target0 = u_xlat10_0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform     vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform     vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform     vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec2 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec2 vs_TEXCOORD0;
layout(location = 0) out mediump vec4 SV_Target0;
lowp vec4 u_xlat10_0;
void main()
{
    u_xlat10_0 = texture(_MainTex, vs_TEXCOORD0.xy);
    SV_Target0 = u_xlat10_0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "vulkan":
Uses vertex data channel "Vertex"
Uses vertex data channel "TexCoord"

Set 2D Texture "_MainTex" to set: 0, binding: 0, used in: Fragment 

Constant Buffer "VGlobals2273603178" (144 bytes) on set: 1, binding: 0, used in: Vertex  {
  Matrix4x4 unity_MatrixVP at 64
  Matrix4x4 unity_ObjectToWorld at 0
  Vector4 _MainTex_ST at 128
}

Shader Disassembly:
Disassembly for Vertex:
// Module Version 10000
// Generated by (magic number): 80001
// Id's are bound by 102

                              Capability Shader
               1:             ExtInstImport  "GLSL.std.450"
                              MemoryModel Logical GLSL450
                              EntryPoint Vertex 4  "main" 11 72 84 86
                              Decorate 11 Location 0
                              Decorate 16 ArrayStride 16
                              Decorate 17 ArrayStride 16
                              MemberDecorate 18 0 Offset 0
                              MemberDecorate 18 1 Offset 64
                              MemberDecorate 18 2 Offset 128
                              Decorate 18 Block
                              Decorate 20 DescriptorSet 1
                              Decorate 20 Binding 0
                              MemberDecorate 70 0 BuiltIn Position
                              MemberDecorate 70 1 BuiltIn PointSize
                              MemberDecorate 70 2 BuiltIn ClipDistance
                              Decorate 70 Block
                              Decorate 84 Location 0
                              Decorate 86 Location 1
               2:             TypeVoid
               3:             TypeFunction 2
               6:             TypeFloat 32
               7:             TypeVector 6(float) 4
               8:             TypePointer Private 7(fvec4)
               9:      8(ptr) Variable Private
              10:             TypePointer Input 7(fvec4)
              11:     10(ptr) Variable Input
              14:             TypeInt 32 0
              15:     14(int) Constant 4
              16:             TypeArray 7(fvec4) 15
              17:             TypeArray 7(fvec4) 15
              18:             TypeStruct 16 17 7(fvec4)
              19:             TypePointer Uniform 18(struct)
              20:     19(ptr) Variable Uniform
              21:             TypeInt 32 1
              22:     21(int) Constant 0
              23:     21(int) Constant 1
              24:             TypePointer Uniform 7(fvec4)
              35:     21(int) Constant 2
              44:     21(int) Constant 3
              48:      8(ptr) Variable Private
              68:     14(int) Constant 1
              69:             TypeArray 6(float) 68
              70:             TypeStruct 7(fvec4) 6(float) 69
              71:             TypePointer Output 70(struct)
              72:     71(ptr) Variable Output
              80:             TypePointer Output 7(fvec4)
              82:             TypeVector 6(float) 2
              83:             TypePointer Output 82(fvec2)
              84:     83(ptr) Variable Output
              85:             TypePointer Input 82(fvec2)
              86:     85(ptr) Variable Input
              96:             TypePointer Output 6(float)
               4:           2 Function None 3
               5:             Label
              12:    7(fvec4) Load 11
              13:    7(fvec4) VectorShuffle 12 12 1 1 1 1
              25:     24(ptr) AccessChain 20 22 23
              26:    7(fvec4) Load 25
              27:    7(fvec4) FMul 13 26
                              Store 9 27
              28:     24(ptr) AccessChain 20 22 22
              29:    7(fvec4) Load 28
              30:    7(fvec4) Load 11
              31:    7(fvec4) VectorShuffle 30 30 0 0 0 0
              32:    7(fvec4) FMul 29 31
              33:    7(fvec4) Load 9
              34:    7(fvec4) FAdd 32 33
                              Store 9 34
              36:     24(ptr) AccessChain 20 22 35
              37:    7(fvec4) Load 36
              38:    7(fvec4) Load 11
              39:    7(fvec4) VectorShuffle 38 38 2 2 2 2
              40:    7(fvec4) FMul 37 39
              41:    7(fvec4) Load 9
              42:    7(fvec4) FAdd 40 41
                              Store 9 42
              43:    7(fvec4) Load 9
              45:     24(ptr) AccessChain 20 22 44
              46:    7(fvec4) Load 45
              47:    7(fvec4) FAdd 43 46
                              Store 9 47
              49:    7(fvec4) Load 9
              50:    7(fvec4) VectorShuffle 49 49 1 1 1 1
              51:     24(ptr) AccessChain 20 23 23
              52:    7(fvec4) Load 51
              53:    7(fvec4) FMul 50 52
                              Store 48 53
              54:     24(ptr) AccessChain 20 23 22
              55:    7(fvec4) Load 54
              56:    7(fvec4) Load 9
              57:    7(fvec4) VectorShuffle 56 56 0 0 0 0
              58:    7(fvec4) FMul 55 57
              59:    7(fvec4) Load 48
              60:    7(fvec4) FAdd 58 59
                              Store 48 60
              61:     24(ptr) AccessChain 20 23 35
              62:    7(fvec4) Load 61
              63:    7(fvec4) Load 9
              64:    7(fvec4) VectorShuffle 63 63 2 2 2 2
              65:    7(fvec4) FMul 62 64
              66:    7(fvec4) Load 48
              67:    7(fvec4) FAdd 65 66
                              Store 48 67
              73:     24(ptr) AccessChain 20 23 44
              74:    7(fvec4) Load 73
              75:    7(fvec4) Load 9
              76:    7(fvec4) VectorShuffle 75 75 3 3 3 3
              77:    7(fvec4) FMul 74 76
              78:    7(fvec4) Load 48
              79:    7(fvec4) FAdd 77 78
              81:     80(ptr) AccessChain 72 22
                              Store 81 79
              87:   82(fvec2) Load 86
              88:     24(ptr) AccessChain 20 35
              89:    7(fvec4) Load 88
              90:   82(fvec2) VectorShuffle 89 89 0 1
              91:   82(fvec2) FMul 87 90
              92:     24(ptr) AccessChain 20 35
              93:    7(fvec4) Load 92
              94:   82(fvec2) VectorShuffle 93 93 2 3
              95:   82(fvec2) FAdd 91 94
                              Store 84 95
              97:     96(ptr) AccessChain 72 22 68
              98:    6(float) Load 97
              99:    6(float) FNegate 98
             100:     96(ptr) AccessChain 72 22 68
                              Store 100 99
                              Return
                              FunctionEnd

Disassembly for Fragment:
// Module Version 10000
// Generated by (magic number): 80001
// Id's are bound by 24

                              Capability Shader
               1:             ExtInstImport  "GLSL.std.450"
                              MemoryModel Logical GLSL450
                              EntryPoint Fragment 4  "main" 17 21
                              ExecutionMode 4 OriginUpperLeft
                              Decorate 9 RelaxedPrecision
                              Decorate 13 RelaxedPrecision
                              Decorate 13 DescriptorSet 0
                              Decorate 13 Binding 0
                              Decorate 14 RelaxedPrecision
                              Decorate 17 Location 0
                              Decorate 21 RelaxedPrecision
                              Decorate 21 Location 0
                              Decorate 22 RelaxedPrecision
               2:             TypeVoid
               3:             TypeFunction 2
               6:             TypeFloat 32
               7:             TypeVector 6(float) 4
               8:             TypePointer Private 7(fvec4)
               9:      8(ptr) Variable Private
              10:             TypeImage 6(float) 2D sampled format:Unknown
              11:             TypeSampledImage 10
              12:             TypePointer UniformConstant 11
              13:     12(ptr) Variable UniformConstant
              15:             TypeVector 6(float) 2
              16:             TypePointer Input 15(fvec2)
              17:     16(ptr) Variable Input
              20:             TypePointer Output 7(fvec4)
              21:     20(ptr) Variable Output
               4:           2 Function None 3
               5:             Label
              14:          11 Load 13
              18:   15(fvec2) Load 17
              19:    7(fvec4) ImageSampleImplicitLod 14 18
                              Store 9 19
              22:    7(fvec4) Load 9
                              Store 21 22
                              Return
                              FunctionEnd

Disassembly for Hull:
Not present.



-- Hardware tier variant: Tier 1
-- Fragment shader for "vulkan":
Shader Disassembly:
 

-- Hardware tier variant: Tier 2
-- Vertex shader for "vulkan":
Uses vertex data channel "Vertex"
Uses vertex data channel "TexCoord"

Set 2D Texture "_MainTex" to set: 0, binding: 0, used in: Fragment 

Constant Buffer "VGlobals2273603178" (144 bytes) on set: 1, binding: 0, used in: Vertex  {
  Matrix4x4 unity_MatrixVP at 64
  Matrix4x4 unity_ObjectToWorld at 0
  Vector4 _MainTex_ST at 128
}

Shader Disassembly:
Disassembly for Vertex:
// Module Version 10000
// Generated by (magic number): 80001
// Id's are bound by 102

                              Capability Shader
               1:             ExtInstImport  "GLSL.std.450"
                              MemoryModel Logical GLSL450
                              EntryPoint Vertex 4  "main" 11 72 84 86
                              Decorate 11 Location 0
                              Decorate 16 ArrayStride 16
                              Decorate 17 ArrayStride 16
                              MemberDecorate 18 0 Offset 0
                              MemberDecorate 18 1 Offset 64
                              MemberDecorate 18 2 Offset 128
                              Decorate 18 Block
                              Decorate 20 DescriptorSet 1
                              Decorate 20 Binding 0
                              MemberDecorate 70 0 BuiltIn Position
                              MemberDecorate 70 1 BuiltIn PointSize
                              MemberDecorate 70 2 BuiltIn ClipDistance
                              Decorate 70 Block
                              Decorate 84 Location 0
                              Decorate 86 Location 1
               2:             TypeVoid
               3:             TypeFunction 2
               6:             TypeFloat 32
               7:             TypeVector 6(float) 4
               8:             TypePointer Private 7(fvec4)
               9:      8(ptr) Variable Private
              10:             TypePointer Input 7(fvec4)
              11:     10(ptr) Variable Input
              14:             TypeInt 32 0
              15:     14(int) Constant 4
              16:             TypeArray 7(fvec4) 15
              17:             TypeArray 7(fvec4) 15
              18:             TypeStruct 16 17 7(fvec4)
              19:             TypePointer Uniform 18(struct)
              20:     19(ptr) Variable Uniform
              21:             TypeInt 32 1
              22:     21(int) Constant 0
              23:     21(int) Constant 1
              24:             TypePointer Uniform 7(fvec4)
              35:     21(int) Constant 2
              44:     21(int) Constant 3
              48:      8(ptr) Variable Private
              68:     14(int) Constant 1
              69:             TypeArray 6(float) 68
              70:             TypeStruct 7(fvec4) 6(float) 69
              71:             TypePointer Output 70(struct)
              72:     71(ptr) Variable Output
              80:             TypePointer Output 7(fvec4)
              82:             TypeVector 6(float) 2
              83:             TypePointer Output 82(fvec2)
              84:     83(ptr) Variable Output
              85:             TypePointer Input 82(fvec2)
              86:     85(ptr) Variable Input
              96:             TypePointer Output 6(float)
               4:           2 Function None 3
               5:             Label
              12:    7(fvec4) Load 11
              13:    7(fvec4) VectorShuffle 12 12 1 1 1 1
              25:     24(ptr) AccessChain 20 22 23
              26:    7(fvec4) Load 25
              27:    7(fvec4) FMul 13 26
                              Store 9 27
              28:     24(ptr) AccessChain 20 22 22
              29:    7(fvec4) Load 28
              30:    7(fvec4) Load 11
              31:    7(fvec4) VectorShuffle 30 30 0 0 0 0
              32:    7(fvec4) FMul 29 31
              33:    7(fvec4) Load 9
              34:    7(fvec4) FAdd 32 33
                              Store 9 34
              36:     24(ptr) AccessChain 20 22 35
              37:    7(fvec4) Load 36
              38:    7(fvec4) Load 11
              39:    7(fvec4) VectorShuffle 38 38 2 2 2 2
              40:    7(fvec4) FMul 37 39
              41:    7(fvec4) Load 9
              42:    7(fvec4) FAdd 40 41
                              Store 9 42
              43:    7(fvec4) Load 9
              45:     24(ptr) AccessChain 20 22 44
              46:    7(fvec4) Load 45
              47:    7(fvec4) FAdd 43 46
                              Store 9 47
              49:    7(fvec4) Load 9
              50:    7(fvec4) VectorShuffle 49 49 1 1 1 1
              51:     24(ptr) AccessChain 20 23 23
              52:    7(fvec4) Load 51
              53:    7(fvec4) FMul 50 52
                              Store 48 53
              54:     24(ptr) AccessChain 20 23 22
              55:    7(fvec4) Load 54
              56:    7(fvec4) Load 9
              57:    7(fvec4) VectorShuffle 56 56 0 0 0 0
              58:    7(fvec4) FMul 55 57
              59:    7(fvec4) Load 48
              60:    7(fvec4) FAdd 58 59
                              Store 48 60
              61:     24(ptr) AccessChain 20 23 35
              62:    7(fvec4) Load 61
              63:    7(fvec4) Load 9
              64:    7(fvec4) VectorShuffle 63 63 2 2 2 2
              65:    7(fvec4) FMul 62 64
              66:    7(fvec4) Load 48
              67:    7(fvec4) FAdd 65 66
                              Store 48 67
              73:     24(ptr) AccessChain 20 23 44
              74:    7(fvec4) Load 73
              75:    7(fvec4) Load 9
              76:    7(fvec4) VectorShuffle 75 75 3 3 3 3
              77:    7(fvec4) FMul 74 76
              78:    7(fvec4) Load 48
              79:    7(fvec4) FAdd 77 78
              81:     80(ptr) AccessChain 72 22
                              Store 81 79
              87:   82(fvec2) Load 86
              88:     24(ptr) AccessChain 20 35
              89:    7(fvec4) Load 88
              90:   82(fvec2) VectorShuffle 89 89 0 1
              91:   82(fvec2) FMul 87 90
              92:     24(ptr) AccessChain 20 35
              93:    7(fvec4) Load 92
              94:   82(fvec2) VectorShuffle 93 93 2 3
              95:   82(fvec2) FAdd 91 94
                              Store 84 95
              97:     96(ptr) AccessChain 72 22 68
              98:    6(float) Load 97
              99:    6(float) FNegate 98
             100:     96(ptr) AccessChain 72 22 68
                              Store 100 99
                              Return
                              FunctionEnd

Disassembly for Fragment:
// Module Version 10000
// Generated by (magic number): 80001
// Id's are bound by 24

                              Capability Shader
               1:             ExtInstImport  "GLSL.std.450"
                              MemoryModel Logical GLSL450
                              EntryPoint Fragment 4  "main" 17 21
                              ExecutionMode 4 OriginUpperLeft
                              Decorate 9 RelaxedPrecision
                              Decorate 13 RelaxedPrecision
                              Decorate 13 DescriptorSet 0
                              Decorate 13 Binding 0
                              Decorate 14 RelaxedPrecision
                              Decorate 17 Location 0
                              Decorate 21 RelaxedPrecision
                              Decorate 21 Location 0
                              Decorate 22 RelaxedPrecision
               2:             TypeVoid
               3:             TypeFunction 2
               6:             TypeFloat 32
               7:             TypeVector 6(float) 4
               8:             TypePointer Private 7(fvec4)
               9:      8(ptr) Variable Private
              10:             TypeImage 6(float) 2D sampled format:Unknown
              11:             TypeSampledImage 10
              12:             TypePointer UniformConstant 11
              13:     12(ptr) Variable UniformConstant
              15:             TypeVector 6(float) 2
              16:             TypePointer Input 15(fvec2)
              17:     16(ptr) Variable Input
              20:             TypePointer Output 7(fvec4)
              21:     20(ptr) Variable Output
               4:           2 Function None 3
               5:             Label
              14:          11 Load 13
              18:   15(fvec2) Load 17
              19:    7(fvec4) ImageSampleImplicitLod 14 18
                              Store 9 19
              22:    7(fvec4) Load 9
                              Store 21 22
                              Return
                              FunctionEnd

Disassembly for Hull:
Not present.



-- Hardware tier variant: Tier 2
-- Fragment shader for "vulkan":
Shader Disassembly:
 

-- Hardware tier variant: Tier 3
-- Vertex shader for "vulkan":
Uses vertex data channel "Vertex"
Uses vertex data channel "TexCoord"

Set 2D Texture "_MainTex" to set: 0, binding: 0, used in: Fragment 

Constant Buffer "VGlobals2273603178" (144 bytes) on set: 1, binding: 0, used in: Vertex  {
  Matrix4x4 unity_MatrixVP at 64
  Matrix4x4 unity_ObjectToWorld at 0
  Vector4 _MainTex_ST at 128
}

Shader Disassembly:
Disassembly for Vertex:
// Module Version 10000
// Generated by (magic number): 80001
// Id's are bound by 102

                              Capability Shader
               1:             ExtInstImport  "GLSL.std.450"
                              MemoryModel Logical GLSL450
                              EntryPoint Vertex 4  "main" 11 72 84 86
                              Decorate 11 Location 0
                              Decorate 16 ArrayStride 16
                              Decorate 17 ArrayStride 16
                              MemberDecorate 18 0 Offset 0
                              MemberDecorate 18 1 Offset 64
                              MemberDecorate 18 2 Offset 128
                              Decorate 18 Block
                              Decorate 20 DescriptorSet 1
                              Decorate 20 Binding 0
                              MemberDecorate 70 0 BuiltIn Position
                              MemberDecorate 70 1 BuiltIn PointSize
                              MemberDecorate 70 2 BuiltIn ClipDistance
                              Decorate 70 Block
                              Decorate 84 Location 0
                              Decorate 86 Location 1
               2:             TypeVoid
               3:             TypeFunction 2
               6:             TypeFloat 32
               7:             TypeVector 6(float) 4
               8:             TypePointer Private 7(fvec4)
               9:      8(ptr) Variable Private
              10:             TypePointer Input 7(fvec4)
              11:     10(ptr) Variable Input
              14:             TypeInt 32 0
              15:     14(int) Constant 4
              16:             TypeArray 7(fvec4) 15
              17:             TypeArray 7(fvec4) 15
              18:             TypeStruct 16 17 7(fvec4)
              19:             TypePointer Uniform 18(struct)
              20:     19(ptr) Variable Uniform
              21:             TypeInt 32 1
              22:     21(int) Constant 0
              23:     21(int) Constant 1
              24:             TypePointer Uniform 7(fvec4)
              35:     21(int) Constant 2
              44:     21(int) Constant 3
              48:      8(ptr) Variable Private
              68:     14(int) Constant 1
              69:             TypeArray 6(float) 68
              70:             TypeStruct 7(fvec4) 6(float) 69
              71:             TypePointer Output 70(struct)
              72:     71(ptr) Variable Output
              80:             TypePointer Output 7(fvec4)
              82:             TypeVector 6(float) 2
              83:             TypePointer Output 82(fvec2)
              84:     83(ptr) Variable Output
              85:             TypePointer Input 82(fvec2)
              86:     85(ptr) Variable Input
              96:             TypePointer Output 6(float)
               4:           2 Function None 3
               5:             Label
              12:    7(fvec4) Load 11
              13:    7(fvec4) VectorShuffle 12 12 1 1 1 1
              25:     24(ptr) AccessChain 20 22 23
              26:    7(fvec4) Load 25
              27:    7(fvec4) FMul 13 26
                              Store 9 27
              28:     24(ptr) AccessChain 20 22 22
              29:    7(fvec4) Load 28
              30:    7(fvec4) Load 11
              31:    7(fvec4) VectorShuffle 30 30 0 0 0 0
              32:    7(fvec4) FMul 29 31
              33:    7(fvec4) Load 9
              34:    7(fvec4) FAdd 32 33
                              Store 9 34
              36:     24(ptr) AccessChain 20 22 35
              37:    7(fvec4) Load 36
              38:    7(fvec4) Load 11
              39:    7(fvec4) VectorShuffle 38 38 2 2 2 2
              40:    7(fvec4) FMul 37 39
              41:    7(fvec4) Load 9
              42:    7(fvec4) FAdd 40 41
                              Store 9 42
              43:    7(fvec4) Load 9
              45:     24(ptr) AccessChain 20 22 44
              46:    7(fvec4) Load 45
              47:    7(fvec4) FAdd 43 46
                              Store 9 47
              49:    7(fvec4) Load 9
              50:    7(fvec4) VectorShuffle 49 49 1 1 1 1
              51:     24(ptr) AccessChain 20 23 23
              52:    7(fvec4) Load 51
              53:    7(fvec4) FMul 50 52
                              Store 48 53
              54:     24(ptr) AccessChain 20 23 22
              55:    7(fvec4) Load 54
              56:    7(fvec4) Load 9
              57:    7(fvec4) VectorShuffle 56 56 0 0 0 0
              58:    7(fvec4) FMul 55 57
              59:    7(fvec4) Load 48
              60:    7(fvec4) FAdd 58 59
                              Store 48 60
              61:     24(ptr) AccessChain 20 23 35
              62:    7(fvec4) Load 61
              63:    7(fvec4) Load 9
              64:    7(fvec4) VectorShuffle 63 63 2 2 2 2
              65:    7(fvec4) FMul 62 64
              66:    7(fvec4) Load 48
              67:    7(fvec4) FAdd 65 66
                              Store 48 67
              73:     24(ptr) AccessChain 20 23 44
              74:    7(fvec4) Load 73
              75:    7(fvec4) Load 9
              76:    7(fvec4) VectorShuffle 75 75 3 3 3 3
              77:    7(fvec4) FMul 74 76
              78:    7(fvec4) Load 48
              79:    7(fvec4) FAdd 77 78
              81:     80(ptr) AccessChain 72 22
                              Store 81 79
              87:   82(fvec2) Load 86
              88:     24(ptr) AccessChain 20 35
              89:    7(fvec4) Load 88
              90:   82(fvec2) VectorShuffle 89 89 0 1
              91:   82(fvec2) FMul 87 90
              92:     24(ptr) AccessChain 20 35
              93:    7(fvec4) Load 92
              94:   82(fvec2) VectorShuffle 93 93 2 3
              95:   82(fvec2) FAdd 91 94
                              Store 84 95
              97:     96(ptr) AccessChain 72 22 68
              98:    6(float) Load 97
              99:    6(float) FNegate 98
             100:     96(ptr) AccessChain 72 22 68
                              Store 100 99
                              Return
                              FunctionEnd

Disassembly for Fragment:
// Module Version 10000
// Generated by (magic number): 80001
// Id's are bound by 24

                              Capability Shader
               1:             ExtInstImport  "GLSL.std.450"
                              MemoryModel Logical GLSL450
                              EntryPoint Fragment 4  "main" 17 21
                              ExecutionMode 4 OriginUpperLeft
                              Decorate 9 RelaxedPrecision
                              Decorate 13 RelaxedPrecision
                              Decorate 13 DescriptorSet 0
                              Decorate 13 Binding 0
                              Decorate 14 RelaxedPrecision
                              Decorate 17 Location 0
                              Decorate 21 RelaxedPrecision
                              Decorate 21 Location 0
                              Decorate 22 RelaxedPrecision
               2:             TypeVoid
               3:             TypeFunction 2
               6:             TypeFloat 32
               7:             TypeVector 6(float) 4
               8:             TypePointer Private 7(fvec4)
               9:      8(ptr) Variable Private
              10:             TypeImage 6(float) 2D sampled format:Unknown
              11:             TypeSampledImage 10
              12:             TypePointer UniformConstant 11
              13:     12(ptr) Variable UniformConstant
              15:             TypeVector 6(float) 2
              16:             TypePointer Input 15(fvec2)
              17:     16(ptr) Variable Input
              20:             TypePointer Output 7(fvec4)
              21:     20(ptr) Variable Output
               4:           2 Function None 3
               5:             Label
              14:          11 Load 13
              18:   15(fvec2) Load 17
              19:    7(fvec4) ImageSampleImplicitLod 14 18
                              Store 9 19
              22:    7(fvec4) Load 9
                              Store 21 22
                              Return
                              FunctionEnd

Disassembly for Hull:
Not present.



-- Hardware tier variant: Tier 3
-- Fragment shader for "vulkan":
Shader Disassembly:
 

 }
}
}